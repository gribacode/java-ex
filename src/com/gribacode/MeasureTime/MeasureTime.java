package com.gribacode.MeasureTime;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MeasureTime {
    public static void main(String[] args) {
        List<Integer> linkedList = new LinkedList<>();
        List<Integer> arrayList = new ArrayList<>();

        // array list is faster on add to the end of the list
        measureTimeOnAddToEnd(linkedList);
        measureTimeOnAddToEnd(arrayList);

        // linked list faster on add to the start of the list
        measureTimeOnAddToStart(linkedList);
        measureTimeOnAddToStart(arrayList);

        // array list is faster on get element from the list
        measureTimeOnGet(linkedList);
        measureTimeOnGet(arrayList);
    }

    private static void measureTimeOnAddToEnd(List<Integer> list) {
        long start = System.currentTimeMillis();

        for (int i = 0; i < 100000; i++) {
            list.add(i);
        }

        long end = System.currentTimeMillis();

        System.out.println(end - start);
    }

    private static void measureTimeOnAddToStart(List<Integer> list) {
        long start = System.currentTimeMillis();

        for (int i = 0; i < 100000; i++) {
            list.add(0, i);
        }

        long end = System.currentTimeMillis();

        System.out.println(end - start);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static void measureTimeOnGet(List<Integer> list) {
        for (int i = 0; i < 100000; i++) {
            list.add(i);
        }

        long start = System.currentTimeMillis();

        for (int j = 0; j < list.size(); j++) {
            list.get(j);
        }

        long end = System.currentTimeMillis();

        System.out.println(end - start);
    }
}
