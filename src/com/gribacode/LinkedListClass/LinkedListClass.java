package com.gribacode.LinkedListClass;

import java.util.Arrays;

// head -> [1] -> [2] -> [3]
public class LinkedListClass<E> {
    private int size;
    private Node head;

    public int getSize() {
        return size;
    }

    public String toString() {
        Object[] toStringArr = new Object[size];

        int idx = 0;
        Node tmp = head;

        while (tmp != null) {
            toStringArr[idx++] = tmp.getValue();
            tmp = tmp.getNext();
        }

        return Arrays.toString(toStringArr);
    }

    public E get(int idx) {
        int currentIdx = 0;
        Node tmp = head;

        while (tmp != null) {
            if (idx == currentIdx) {
                return tmp.getValue();
            } else {
                tmp = tmp.getNext();
                currentIdx++;
            }
        }
        throw new IllegalArgumentException("Segmentation fault");
    }

    public void add(E value) {
        // if this is the first addition to the list
        if (head == null) {
            this.head = new Node(value);
        } else {
            Node tmp = head;

            // tmp points to the last element of the list
            while (tmp.getNext() != null) {
                tmp = tmp.getNext();
            }
            tmp.setNext(new Node(value));
        }
        size++;
    }

    public void remove(int idx) {
        if (idx == 0) {
            this.head = head.getNext();
            size--;
            return;
        }

        int currentIdx = 0;
        Node tmp = head;

        while (tmp != null) {
            // head -> [1] getNext -> [2] getNext -> [3] -> null
            if (idx - 1 == currentIdx) {
                tmp.setNext(tmp.getNext().getNext());
                size--;
                return;
            } else {
                tmp = tmp.getNext();
                currentIdx++;
            }
        }
        throw new IllegalArgumentException("Segmentation fault");
    }

    private class Node {
        private E value;
        private Node next;

        public Node(E value) {
            this.value = value;
        }

        public E getValue() {
            return value;
        }

        public void setValue(E value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }
}
