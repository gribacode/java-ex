package com.gribacode.LinkedListClass;

public class Main {
    public static void main(String[] args) {
        LinkedListClass<Integer> linkedListClass = new LinkedListClass<>();

        for (int i = 0; i < 20; i++) {
            linkedListClass.add(i);
        }

        System.out.println(linkedListClass);
        System.out.println(linkedListClass.getSize());

        linkedListClass.remove(1);

        System.out.println(linkedListClass);
        System.out.println(linkedListClass.getSize());

        System.out.println(linkedListClass.get(7));

    }
}
